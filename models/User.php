<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_CREATE = 'create';

    public static function tableName()
    {
        return 'user';
    }

    public function getTasks(){
        return $this->hasMany(Task::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    public function getTaskLists(){
        return $this->hasMany(TaskList::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username', 'password'], 'string', 'min' => 3],
            ['email', 'email'],
            ['tasklist_limit', 'integer'],
            ['password', 'validateLogin', 'on' => self::SCENARIO_LOGIN],
            ['username', 'validateUsername', 'on' => self::SCENARIO_CREATE],
        ];
    }

    public function validateLogin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findOne(['username' => $this->username]);

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function validateUsername($attribute, $params){
        if (!$this->hasErrors()) {
            if(User::findOne(['username' => $this->username])){
                $this->addError($attribute, 'username already exist');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function hashPassword($password)
    {
        $this->password = \Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function genAccessToken(){
        $this->access_token = \Yii::$app->getSecurity()->generateRandomString();
        return $this->access_token;
    }

}

