<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 10.08.18
 * Time: 9:40
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Task;
use app\models\User;

class TaskList extends ActiveRecord
{
    public static function tableName()
    {
        return 'tasklist';
    }

    public function getTasks(){
        return $this->hasMany(Task::className(), ['tasklist_id' => 'id'])->inverseOf('tasklist');
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            ['name', 'string', 'max' => 255],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
            ['user_id', 'validateUserLimit']
        ];
    }

    public function validateUserLimit($attribute, $params){
        if (!$this->hasErrors()) {
            $user = User::findOne(['id' => $this->user_id]);
            $tasklist_count = count($user->taskLists);
            if($user->tasklist_limit <= $tasklist_count){
                $this->addError($attribute, 'tasklist limit reached');
            }
        }
    }
}