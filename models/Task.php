<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 10.08.18
 * Time: 9:40
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\TaskList;
use app\models\User;

class Task extends ActiveRecord
{
    public static function tableName()
    {
        return 'task';
    }

    public function getTaskList(){
        return $this->hasOne(TaskList::className(), ['id' => 'tasklist_id']);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function rules()
    {
        return [
            [['description', 'tasklist_id', 'user_id'], 'required'],
            ['tasklist_id', 'exist', 'targetClass' => TaskList::class, 'targetAttribute' => 'id'],
            ['user_id', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'id'],
        ];
    }
}