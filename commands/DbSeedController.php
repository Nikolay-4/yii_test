<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\TaskList;
use app\models\User;
use app\models\Task;
use yii\helpers\VarDumper;

class DbSeedController extends Controller
{
    public function actionUser()
    {
        $u1 = new User();
        $u1->username = "user1";
        $u1->hashPassword('123');
        $u1->genAccessToken();
        $u1->save();

        $u2 = new User();
        $u2->username = "user2";
        $u2->hashPassword('123');
        $u2->genAccessToken();
        $u2->save();

        $a1 = new User();
        $a1->username = "admin1";
        $a1->hashPassword('123');
        $a1->genAccessToken();
        $a1->save();

        $s1 = new User();
        $s1->username = "superadmin1";
        $s1->hashPassword('123');
        $s1->genAccessToken();
        $s1->save();

        $auth = Yii::$app->authManager;

        // добавляем разрешение "crudTask"
        $crudTask = $auth->createPermission('crudTask');
        $crudTask->description = 'create, update, delete a task';
        $auth->add($crudTask);

        // добавляем разрешение "crudTasklist"
        $crudTaskList = $auth->createPermission('crudTaskList');
        $crudTaskList->description = 'create, update, delete a task list';
        $auth->add($crudTaskList);

        $adminPanel= $auth->createPermission('adminPanel');
        $adminPanel->description = 'access to admin panel';
        $auth->add($adminPanel);

        $editTaskListLimit= $auth->createPermission('editTaskListLimit');
        $editTaskListLimit->description = 'edit task list limit';
        $auth->add($editTaskListLimit);

        // добавляем роль "user" и даём роли разрешение "crud task, tasklist"
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $crudTask);
        $auth->addChild($user, $crudTaskList);

        // добавляем роль "admin"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $adminPanel);
        $auth->addChild($admin, $user); // добавление прав пользователя

        $superAdmin = $auth->createRole('super_admin');
        $auth->add($superAdmin);
        $auth->addChild($superAdmin, $editTaskListLimit);
        $auth->addChild($superAdmin, $admin);


        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        // обычно реализуемый в модели User.
        $auth->assign($user, $u1->id);
        $auth->assign($user, $u2->id);
        $auth->assign($admin, $a1->id);
        $auth->assign($superAdmin, $s1->id);
    }

    public function actionTask(){
        $u1 = User::findOne(['username' => 'user1']);
        $u2 = User::findOne(['username' => 'user2']);

        $tl1 = TaskList::findOne(['name' => 'tasklist1']);
        $tl2 = TaskList::findOne(['name' => 'tasklist2']);

        $t1 = new Task();
        $t1->description = 'task1';

        //????
//        $t1->link('taskList', $tl1);
//        $t1->link('user', $u1);

        $t1->tasklist_id = $tl1->id;
        $t1->user_id = $u1->id;
        $t1->save();

        $t2 = new Task();
        $t2->description = 'task2';
        $t2->tasklist_id = $tl2->id;
        $t2->user_id = $u2->id;
        $t2->save();

        $t3 = new Task();
        $t3->description = 'task3';
        $t3->tasklist_id = $tl2->id;
        $t3->user_id = $u2->id;
        $t3->save();

    }

    public function actionTaskList(){
        $u1 = User::findOne(['username' => 'user1']);
        $u2 = User::findOne(['username' => 'user2']);

        $tl1 = new TaskList();
        $tl1->name = 'tasklist1';
        $tl1->link('user', $u1);
        $tl1->save();

        $tl2 = new TaskList();
        $tl2->name = 'tasklist2';
        $tl2->link('user', $u2);
        $tl2->save();

        $tl3 = new TaskList();
        $tl3->name = 'tasklist3';
        $tl3->link('user', $u2);
        $tl3->save();
    }
}