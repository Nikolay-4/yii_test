<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Task Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-list-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Task List', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'user_id',
                [
                    'attribute' => 'tasks',
                    'format' => 'raw',
                    'value' => function($model){
                        return Html::a(
                            'show',
                            Url::to(['/admin/task', 'tasklist_id' => $model['id']])
                        );
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
