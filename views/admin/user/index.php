<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-list-index box box-primary">
    <div class="box-body table-responsive no-padding">
        <?php
        if(Yii::$app->user->can('editTaskListLimit'))
        {
//            ?????
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'username',
                    'email',
                    'tasklist_limit',

                    [
                        'attribute' => 'task lists',
                        'format' => 'raw',
                        'value' => function($model){
                            return Html::a(
                                'show',
                                Url::to(['/admin/tasklist', 'user_id' => $model['id']])
                            );
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                    ],
                ],
            ]);
        }
        else
        {
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'username',
                    'email',
                    'tasklist_limit',
                    [
                        'attribute' => 'task lists',
                        'format' => 'raw',
                        'value' => function($model){
                            return Html::a(
                                    'show',
                                    Url::to(['/admin/tasklist', 'user_id' => $model['id']])
                            );
                        }
                    ],
                ],
            ]);
        } ?>
    </div>
</div>
