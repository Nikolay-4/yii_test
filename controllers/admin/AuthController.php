<?php

namespace app\controllers\admin;

use app\models\User;
use Yii;
use yii\helpers\Url;
use app\models\Task;
use app\models\TaskSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class AuthController extends Controller
{

    public function actionLogin(){

        $this->layout = 'login';
        $model = new User();
        $model->setScenario('login');

        if(\Yii::$app->request->method == 'GET'){
            return $this->render('login', ['model' => $model]);
        }

        $model->attributes = \Yii::$app->request->post('User');
        if($model->validate()){
            Yii::$app->user->login(User::findOne(['username' => $model->username]));

            return $this->redirect('/admin');
        } else{
            return $this->render('login', ['model' => $model]);
        }

    }

    public function behaviors()
    {
        return [
            [
                'class' => 'app\filters\RbacFilter',
                'action_permission' =>[
                    'logout' => 'adminPanel',
                ]
            ],
        ];
    }

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->redirect(Url::toRoute('login'));
    }
}
