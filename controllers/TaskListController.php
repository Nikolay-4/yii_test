<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 09.08.18
 * Time: 14:04
 */

namespace app\controllers;

use app\models\TaskList;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Controller;
use app\models\User;
use yii\web\Response;

class TaskListController extends ActiveController
{
    public $modelClass = 'app\models\TaskList';

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['index', 'view']
            ],
            [
                'class' => 'app\filters\RbacFilter',
                'action_permission' => [
                    'create' => 'crudTaskList',
                    'update' => 'crudTaskList',
                    'delete' => 'crudTaskList',
                ]
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['options']);
        unset($actions['create']);
        return $actions;
    }

    public function actionCreate(){
        $request = \Yii::$app->request;

        $tasklist = new TaskList();
        $tasklist->name = $request->post('name');
        $tasklist->user_id = \Yii::$app->user->id;

        if(!$tasklist->validate()){
            return $tasklist->errors;
        }
        $tasklist->save();
        return $tasklist;
    }
}