<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 14.08.18
 * Time: 10:13
 */

namespace app\controllers;
use app\models\User;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\web\HttpException;

class LoginController extends Controller
{
    public function actionAuth(){
        $request = \Yii::$app->request;

        $username = $request->post('username');
        $password = $request->post('password');
        $model = DynamicModel::validateData(['username' => $username, 'password' => $password], [
            [['username', 'password'], 'required'],
            ['username', 'exist', 'targetClass' => User::class, 'targetAttribute' => 'username'],
        ]);

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($model->hasErrors()) {
            \Yii::$app->response->statusCode = 400;
            return $model->errors;
        }

        $user = User::findOne(['username' => $username]);
        if(!$user->validatePassword($password)){
            \Yii::$app->response->statusCode = 400;
            return ['errors' => 'invalid login or password'];
        }
        return ['token' => $user->access_token];
    }
}