<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 09.08.18
 * Time: 14:04
 */

namespace app\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Controller;
use app\models\User;
use app\models\Task;
use yii\web\HttpException;
use yii\web\Response;

class TaskController extends ActiveController
{
    public $modelClass = 'app\models\Task';

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'except' => ['index', 'view']
            ],
            [
                'class' => 'app\filters\RbacFilter',
                'action_permission' => [
                    'create' => 'crudTask',
                    'update' => 'crudTask',
                    'delete' => 'crudTask',
                ]
            ],
        ];
    }


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['options']);
        unset($actions['create']);
        return $actions;
    }

    public function actionCreate(){
        $request = \Yii::$app->request;

        $task = new Task();
        $task->description = $request->post('description');
        $task->tasklist_id = $request->post('tasklist_id');
        $task->user_id = \Yii::$app->user->id;

        if(!$task->validate()){
            return $task->errors;
        }
        $task->save();
        return $task;
    }
}