<?php

use yii\db\Migration;
/**
 * Handles the creation of table `task`.
 */
class m180810_063100_create_task_table extends Migration
{
//m180810_060129_create_task_table
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'description' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'tasklist_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-task-user_id',
            'task',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-task-tasklist_id',
            'task',
            'tasklist_id',
            'tasklist',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
