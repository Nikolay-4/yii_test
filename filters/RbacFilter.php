<?php
/**
 * Created by PhpStorm.
 * User: linux
 * Date: 16.08.18
 * Time: 11:01
 */

namespace app\filters;


use yii\base\ActionFilter;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class RbacFilter extends ActionFilter
{
    public $action_permission = [];
    public function beforeAction($action)
    {
        if(isset($this->action_permission[$action->id]) and
            !\Yii::$app->user->can($this->action_permission[$action->id])){

//            \Yii::$app->response->redirect(Url::toRoute('/admin/auth/login'), 301)->send();
//            return false;
            throw new ForbiddenHttpException('you have not permission');
        }
        return parent::beforeAction($action);
    }
}