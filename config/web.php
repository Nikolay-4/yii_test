<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'uY_WmIwVfrsSQlbpYEsufVT0-d320yhn',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
//        'response' => [
//            'formatters' => [
//                \yii\web\Response::FORMAT_JSON =>[
//                    'class' => 'yii\web\JsonResponseFormatter',
//                ]
//            ]
//        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
//            'errorAction' => 'admin/user/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
//            ?????
            'rules' => [

                'admin/login' => 'admin/auth/login',
                'admin/logout' => 'admin/auth/logout',

                'admin' => 'admin/task/index',
                'admin/task/view' => 'admin/task/view',
                'admin/task/update' => 'admin/task/update',
                'admin/task/delete' => 'admin/task/delete',
                'admin/task/create' => 'admin/task/create',

                'admin/tasklist' => 'admin/task-list/index',
                'admin/tasklist/view' => 'admin/task-list/view',
                'admin/tasklist/update' => 'admin/task-list/update',
                'admin/tasklist/delete' => 'admin/task-list/delete',
                'admin/tasklist/create' => 'admin/task-list/create',

                'admin/user' => 'admin/user/index',
                'admin/user/update' => 'admin/user/update',

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'task',
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'task-list',
                ],
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ]

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
//        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
        'generators' => [ //here
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                ]
            ]
        ],
    ];
}

return $config;
